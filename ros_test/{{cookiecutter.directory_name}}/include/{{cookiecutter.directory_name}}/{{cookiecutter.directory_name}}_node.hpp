#include "rclcpp/rclcpp.hpp"

class {{cookiecutter.class_name}} : public rclcpp::Node
{
public:
  {{cookiecutter.class_name}}();

private:
  void publish();
};
